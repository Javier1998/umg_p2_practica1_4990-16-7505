package practicano1;

import java.util.Scanner;

public class PracticaNo1 {


    public static void main(String[] args) {
        
        int op1, op2; 
        Double lado, radio, area, diametro, pi=3.14, circunferencia, arista, perimetro, longitud;
        String PRIMER_NOMBRE, SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO;
        Scanner leer = new Scanner(System.in);
        System.out.println("Digita tu primer nombre");
        PRIMER_NOMBRE = leer.nextLine();
        System.out.println("Digita tu segundo nombre");
        SEGUNDO_NOMBRE = leer.nextLine();
        System.out.println("Digita tu primer apellido");
        PRIMER_APELLIDO = leer.nextLine();
        System.out.println("Digita tu segundo apellido");
        SEGUNDO_APELLIDO = leer.nextLine();
        System.out.println("Bienvenido a este programa Señ@r \n" +PRIMER_APELLIDO +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + SEGUNDO_NOMBRE);
        System.out.println("1.Calculo de Area \n2.Calculo de Diametro");
        op1 = leer.nextInt();
        switch (op1) {
            case 1:
                //////////////////  AREA //////////
                System.out.println("A que figura le deseas calcular el area \n1.Esfera\n2.Cubo \n3.Circulo \n4.Cuadrado");
                op2 = leer.nextInt();
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa el radio de la esfera");
                        radio = leer.nextDouble();
                        diametro= radio * radio;
                        area = 4*pi*diametro;
                        System.out.println("Señ@r "+ PRIMER_APELLIDO + " "+ SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area equivale a "+area+"cm2");
                        
                        break;
                    case 2:
                        System.out.println("Ingresa la medida de un lado del cubo");
                        lado = leer.nextDouble();
                        arista = lado * lado;
                        area = arista*6;
                        System.out.println("Señ@r "+ PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area del cubo equivale a "+area+"cm");
                        break;
                    case 3:  
                        System.out.println("Ingrese el radio del circulo");
                        radio = leer.nextDouble();
                        diametro= radio * radio;
                        area = pi*diametro;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area equivale a "+area+"cm2");
                        break;
                    case 4:
                        System.out.println("Ingresa la medida de un lado del cuadrado");
                        lado = leer.nextDouble();
                        area = lado * lado;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area del cubo equivale a "+area+"cm2");
                default:
                    System.out.println("La opcion digitada es incorrecta");
                }
                break;
                //////////////////// DIAMETRO Y PERIMETRO /////////////////////
            case 2:
                System.out.println("A que figura le deseas calcular el siametro \n1.Esfera\n2.Cubo \n3.Circulo \n4.Cuadrado");
                op2 = leer.nextInt();
                switch (op2) {
                    case 1:
                        System.out.println("Ingresa la circunferencia de la esfera");
                        circunferencia = leer.nextDouble();
                        radio= circunferencia/(2*pi);
                        diametro = radio * radio;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El diametro equivale a "+diametro);                       
                        break;
                    case 2:
                        System.out.println("Ingresa la medida de una arista");
                        arista = leer.nextDouble();
                        perimetro = arista * 12;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El perimetro del cubo equivale a "+perimetro);
                        break;
                    case 3:  
                        System.out.println("Ingrese el radio del circulo");
                        radio = leer.nextDouble();
                        diametro= radio * radio;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " + SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area equivale a "+diametro);
                        break;
                    case 4:
                        System.out.println("Ingresa la longitud de un lado del cuadrado");
                        longitud = leer.nextDouble();
                        perimetro = longitud * 4;
                        System.out.println("Señ@r "+PRIMER_APELLIDO + " " +SEGUNDO_APELLIDO + ", " + PRIMER_NOMBRE + " " + SEGUNDO_NOMBRE);
                        System.out.println("El area del cubo equivale a "+perimetro);
                default:
                    System.out.println("La opcion digitada es incorrecta");
                }
            default:
                System.out.println("La opcion digitada es incorrecta");
        }
        
    }
    
}
